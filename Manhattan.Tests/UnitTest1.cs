namespace Manhattan.Tests;
using Xunit;
using global::Manhattan;
public class ManhattanDistanceTests
{
    [Fact]
    public void ShouldReturnZeroWhenSamePoint()
    {
        var manhattanDistance = new Manhattan();
        var actual = manhattanDistance.Distance(new Point(1, 1), new Point(1, 1));
        Assert.Equal(0, actual);
    }
    [Fact]
    public void ShouldComputeDistanceOnSameColumn()
    {
        var manhattanDistance = new Manhattan();
        var actual = manhattanDistance.Distance(new Point(1, 1), new Point(1, 2));
        Assert.Equal(1, actual);
    }

    [Fact]
    public void ShouldComputeDistanceOnSameLine()
    {
        var manhattanDistance = new Manhattan();
        var actual = manhattanDistance.Distance(new Point(1, 1), new Point(3, 1));
        Assert.Equal(2, actual);
    }

    [Fact]

    public void ShouldComputeDistanceOnManyColumnsAndLines()
    {
        var manhattanDistance = new Manhattan();
        var actual = manhattanDistance.Distance(new Point(5, 4), new Point(3, 2));
        Assert.Equal(4, actual);
        actual = manhattanDistance.Distance(new Point(1, 1), new Point(0, 3));
        Assert.Equal(3, actual);
        actual = manhattanDistance.Distance(new Point(1, 1), new Point(0, -3));
        Assert.Equal(5, actual);
        actual = manhattanDistance.Distance(new Point(-1, -1), new Point(-2, -3));
        Assert.Equal(3, actual);
    }

    [Theory]
    [InlineData(5, 4, 3, 2, 4)]
    public void ShouldComputeManhattanDistance(int ax, int ay, int bx, int by, int expected)
    {
        var manhattanDistance = new Manhattan();
        var actual = manhattanDistance.Distance(new Point(ax, ay), new Point(bx, by));
        Assert.Equal(expected, actual);

    }
}