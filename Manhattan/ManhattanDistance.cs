﻿namespace Manhattan;
public class Manhattan
{
    public int Distance(Point origin, Point destination)
    {
        return origin.Distance(destination);
    }
}

public class Point
{
    private int x;
    private int y;
    public Point(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    public int Distance(Point other)
    {
        return Math.Abs(x - other.x) + Math.Abs(y - other.y);
    }
}


